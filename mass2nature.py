# -*- coding: utf-8 -*-
"""
Created on Thu Jun 01 13:01:30 2017

@author: s.katnagallu
"""
from APTessentials import *
import pandas as pd
epos = readepos('C:\Users\s.katnagallu\Desktop\Project Evil\APT/R25_19242-v01.epos')
[ions,rr]=read_rrng('C:\Users\s.katnagallu\Desktop\Project Evil\APT/kappaCarbides.rrng')
pos = pd.DataFrame({'x':epos['x'],
                    'y':epos['y'],
                    'z':epos['z'],
                    'Da':epos['m']})
    
def label_ions_nature2(pos,rr,ions): 
    pos['volume']=''
    pos['nature']=''
    for n, r in rr.iterrows():
        count=0
        pos.loc[(pos.Da >= r.lower) & (pos.Da <= r.upper),['volume']] = [r['vol']]
        for ii in ions.name:
            count= count+1
            if ii in rr.comp[n]:
                pos.loc[(pos.Da >= r.lower) & (pos.Da <= r.upper),['nature']]=[count]
    pos.loc[pos.nature == '',['nature']]=count+1
    return pos


