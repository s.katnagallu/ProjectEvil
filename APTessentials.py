
# coding: utf-8

# In[14]:

import numpy as np
from pynx import gpu
import warnings
import pandas as pd
import struct
import re
warnings.filterwarnings('ignore')


# In[15]:

def readepos(file_name):
    f = open(file_name, 'rb')
    dt_type = np.dtype({'names':['x', 'y', 'z', 'm', 't', 'vdc', 'vp', 'dx', 'dy', 'nulls', 'Nat_pulse'], 
                  'formats':['>f4', '>f4', '>f4', '>f4', '>f4', '>f4', '>f4', '>f4', '>f4', '>i4', '>i4']})
    epos = np.fromfile(f, dt_type, -1)
    f.close()
    return epos


# In[16]:

def read_rrng(f):
    rf = open(f,'r').readlines()

    patterns = re.compile(r'Ion([0-9]+)=([A-Za-z0-9]+).*|Range([0-9]+)=(\d+.\d+) +(\d+.\d+) +Vol:(\d+.\d+) +([A-Za-z:0-9 ]+) +Color:([A-Z0-9]{6})')

    ions = []
    rrngs = []
    for line in rf:
        m = patterns.search(line)
        if m:
            if m.groups()[0] is not None:
                ions.append(m.groups()[:2])
            else:
                rrngs.append(m.groups()[2:])

    ions = pd.DataFrame(ions, columns=['number','name'])
    ions.set_index('number',inplace=True)
    rrngs = pd.DataFrame(rrngs, columns=['number','lower','upper','vol','comp','colour'])
    rrngs.set_index('number',inplace=True)
    
    rrngs[['lower','upper','vol']] = rrngs[['lower','upper','vol']].astype(float)
    rrngs[['comp','colour']] = rrngs[['comp','colour']].astype(str)
    
    return ions,rrngs


# In[17]:

def read_pos(f):
    # read in the data
    with open(f,mode='rb') as file:
        fileContent = file.read()
        n = len(fileContent)/4
        d = struct.unpack('>'+'f'*n,fileContent) 
                        # '>' denotes 'big-endian' byte order
        # unpack data
        pos = pd.DataFrame({'x': d[0::4],
                            'y': d[1::4],
                            'z': d[2::4],
                            'Da': d[3::4]})
        return pos


# In[18]:

def label_ions(pos,rrngs):
    pos['comp'] = ''
    pos['colour'] = '#FFFFFF'
    
    for n,r in rrngs.iterrows():
        pos.loc[(pos.Da >= r.lower) & (pos.Da <= r.upper),['comp','colour']] = [r['comp'],'#' + r['colour']]
    
    return pos


# In[19]:

def cart2pol(x, y):
    rho = np.sqrt(x**2 + y**2)
    phi = np.arctan2(y,x)
    return(rho, phi)

def pol2cart(rho, phi):
    x = rho * np.cos(phi)
    y = rho * np.sin(phi)
    return x, y


# In[20]:

def build_recon_ktheta(detector_x, detector_y, v, kf, k_theta,detector_efficiency,evaporation_field ,atomic_volume):
    
    detector_x=detector_x+((np.random.rand(detector_x.size)-0.5)*0.05)
    detector_y=detector_y+((np.random.rand(detector_y.size)-0.5)*0.05)
    specimen_radius=v/(kf*evaporation_field); # calculates the specimen radius in nm
    [detector_r,detector_psi]=cart2pol(detector_x*1e-3,detector_y*1e-3);
    detector_radius=np.max(detector_r); #in m
    detector_theta=detector_radius/k_theta;


    specimen_theta=detector_r/k_theta;

    specimen_x=specimen_radius*np.sin(specimen_theta)*np.cos(detector_psi); #in nm
    specimen_y=specimen_radius*np.sin(specimen_theta)*np.sin(detector_psi); #in nm
    specimen_z=specimen_radius*(1-np.cos(specimen_theta)); #in nm


    analysed_area=np.pi*(specimen_radius**2)*(np.sin(detector_theta)**2); #in nm^2
 
    specimen_dz=-(atomic_volume*np.ones(detector_x.shape))/(detector_efficiency*analysed_area); # computesin-depth increment for each atom,in nm
    specimen_dz=np.cumsum(specimen_dz);      # computes the cumulative sum of the in-depth increment
    specimen_z=-specimen_z+(specimen_dz);     # add the in-depth increment to the z position
    specimen_z=specimen_z-np.max(specimen_z);       # recentres the data in z
    specimen_z=-specimen_z;       # make the z positive
    
    return list(specimen_x),list(specimen_y),list(specimen_z)

def build_recon_icf(detector_x, detector_y, v, kf, icf,detector_efficiency,evaporation_field ,atomic_volume, L):
    
    detector_x=detector_x+((np.random.rand(detector_x.size)-0.5)*0.05)
    detector_y=detector_y+((np.random.rand(detector_y.size)-0.5)*0.05)
    specimen_radius=v/(kf*evaporation_field); # calculates the specimen radius in nm
    [detector_r,detector_psi]=cart2pol(detector_x*1e-3,detector_y*1e-3);
    detector_radius=np.max(detector_r); #in m
   

    thetaP=np.arctan2(detector_r,L)
    theta = thetaP + np.arcsin((icf-1)*np.sin(thetaP))
    [zP,d] =pol2cart(theta,specimen_radius)
    [specimen_x,specimen_y] = pol2cart(detector_psi,d)

    zP =specimen_radius - zP
 
    analysed_area=np.pi*(detector_radius**2) #in nm^2
 
    specimen_dz=-atomic_volume/(detector_efficiency*analysed_area); # computesin-depth increment for each atom,in nm
    specimen_dz=np.cumsum(specimen_dz);      # computes the cumulative sum of the in-depth increment
    specimen_z=zP+(specimen_dz);     # add the in-depth increment to the z position

    
    return list(specimen_x),list(specimen_y),list(specimen_z)

def build_recon_Bas(detector_x,detector_y,v,kf,icf,detector_efficiency,evaporation_field,atomic_volume,screen_distance):
    
    detector_x=detector_x+((np.random.rand(detector_x.size)-0.5)*0.05)
    detector_y=detector_y+((np.random.rand(detector_y.size)-0.5)*0.05)
    specimen_radius=v/(kf*evaporation_field); # calculates the specimen radius in nm
    
    [detector_psi,detector_r]=cart2pol(detector_x*1e-3,detector_y*1e-3);
    detector_radius=np.max(detector_r); #in m
    Sd=np.pi*detector_radius**2
    
    Mproj=screen_distance*1e-3*1e9/(icf*specimen_radius)
    specimen_x=detector_x*1e-3*1e9/Mproj
    specimen_y=detector_y*1e-3*1e9/Mproj
    
    specimen_dz=-(atomic_volume*screen_distance**2 *1e-6*1e18 * kf**2 * evaporation_field**2)/(detector_efficiency*Sd*1e18*icf**2 * v**2)
    
    specimen_dz_prime=specimen_radius*(1-np.sqrt(1-((specimen_x**2 + specimen_y**2)/specimen_radius**2)))
    
    specimen_z = -(np.cumsum(specimen_dz) + specimen_dz_prime)
    
    
    return specimen_x,specimen_y,specimen_z

def build_recon_katnagallu(detector_x, detector_y, v, kf,K_theta,detector_efficiency,evaporation_field ,Nplane,dhkl,refcoord):
    detector_x=detector_x+((np.random.rand(detector_x.size)-0.5)*0.05)
    detector_y=detector_y+((np.random.rand(detector_y.size)-0.5)*0.05)
    specimen_radius=v/(kf*evaporation_field); # calculates the specimen radius in nm
    [detector_r,detector_psi]=cart2pol(detector_x*1e-3,detector_y*1e-3);


    specimen_theta=detector_r/K_theta;

    specimen_x=specimen_radius*np.sin(specimen_theta)*np.cos(detector_psi); #in nm
    specimen_y=specimen_radius*np.sin(specimen_theta)*np.sin(detector_psi); #in nm
    specimen_z=specimen_radius*(1-np.cos(specimen_theta)); #in nm


   
    specimen_dz=(((Nplane*dhkl*detector_efficiency)/(detector_x.size))/(np.cos(np.linalg.norm(refcoord*1e-3)/K_theta)))*np.ones(detector_x.shape); # computesin-depth increment for each atom,in nm
    specimen_dz=np.cumsum(specimen_dz);      # computes the cumulative sum of the in-depth increment
    specimen_z=-specimen_z-(specimen_dz);     # add the in-depth increment to the z position
    specimen_z=specimen_z-np.max(specimen_z);       # recentres the data in z
    specimen_z=-specimen_z;       # make the z positive
    
    return specimen_x,specimen_y,specimen_z


# In[21]:

def visualisation_plotly(xk,yk,zk,mk):
    import plotly.plotly as py
    import plotly.graph_objs as go
    import numpy as np

    x=np.asarray(xk)
    y=np.asarray(yk)
    z=np.asarray(zk)
    m=np.asarray(mk)
    temp=np.random.permutation(np.size(xk))
    temp=temp[1:40000]
    trace1 = go.Scatter3d(
        x=x[temp],
        y=y[temp],
        z=z[temp],
        mode='markers',
        marker=dict(
            size=2,
            color=m[temp],                # set color to an array/list of desired values
            colorscale='Viridis',   # choose a colorscale
            opacity=0.9
        )
    )

    data = [trace1]
    layout = go.Layout(
        margin=dict(
            l=0,
            r=0,
            b=0,
            t=0
        )
    )
    fig = go.Figure(data=data, layout=layout)
    py.iplot(fig, filename='3d-scatter-colorscale')


# In[22]:

def fourier3D(x,y,z):
    lattice_parameter=input('Enter the lattice parameter in nm ')
    xEl = x
    yEl = y
    zEl = z
    # Convert to fractional coordinates
    #lattice_parameter = latticeParameter  # change it accordingly to your choice of crystals 
    xEl/= lattice_parameter
    yEl/= lattice_parameter
    zEl/= lattice_parameter
    xEl.max(), xEl.min()  # quick check
    print("The volume of the cube - in fractional coordinates - is roughly: ",          "%10.3E"% (xEl.max() - xEl.min())**3, " nmˆ3")
    h=np.arange(-3.1,3.1,0.1)
    k=np.arange(-3.1,3.1,0.1)[:,np.newaxis]
    l=np.arange(-3.1,3.1,0.1)[:,np.newaxis,np.newaxis]
    # The actual computation
    fhkl,dt=gpu.Fhkl_thread(h,k,l,xEl,yEl,zEl,gpu_name="2200")
    print("The Fourier Transform computation time is :", dt)
    print("Shape of fhkl vector", np.shape(fhkl))
    surf_eq = abs(np.log10(abs(fhkl)**2))
    print("max and min", surf_eq.max(), surf_eq.min())
    print(type(surf_eq), surf_eq.size, surf_eq.shape)
    iso_value = (surf_eq.max() + surf_eq.min())/2.
    
    return fhkl,surf_eq,iso_value






